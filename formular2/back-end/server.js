const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255), cnp  VARCHAR(20), gen VARCHAR(3), varsta VARCHAR(3), email  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    gen: "",
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
  };
  let error = [];
  
  
  if(!bilet.nume||!bilet.prenume||!bilet.telefon||!bilet.cnp||!bilet.email||!bilet.data_inceput||!bilet.data_sfarsit||!bilet.varsta){
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (bilet.nume.length < 3 && bilet.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!bilet.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (bilet.prenume.length < 3 || bilet.prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!bilet.email.includes("@gmail.com") && !bilet.email.includes("@yahoo.com")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    if (!bilet.data_inceput.match("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$") || !bilet.data_sfarsit.match("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$")) {
      console.log("Data trebuie sa fie de forma zz/ll/aaaa!");
      error.push("Data trebuie sa fie de forma zz/ll/aaaa!");
    } else {
      var data1 = bilet.data_inceput.split('/');
      var dd1  = parseInt(data1[0]);
      var mm1 = parseInt(data1[1]);
      var yy1 = parseInt(data1[2]);
      var data2 = bilet.data_sfarsit.split('/');
      var dd2  = parseInt(data2[0]);
      var mm2 = parseInt(data2[1]);
      var yy2 = parseInt(data2[2]);
  
      var data_curenta = new Date();
      var inceput = new Date(+data1[2], data1[1] -1, +data1[0] +1);
      var sfarsit = new Date(+data2[2], data2[1] -1, +data2[0] +1);
    }
    if (yy1 > yy2) {
      console.log("Anul datei de inceput trebuie sa fie mai mic sau la fel cu anul datei de sfarsit!");
      error.push("Anul datei de inceput trebuie sa fie mai mic sau la fel cu anul datei de sfarsit!");
    } else if(yy1 == yy2) {
      if (mm1 > mm2) {
        console.log("Luna datei de inceput trebuie sa fie mai mica sau la fel cu luna datei de sfarsit!");
        error.push("Luna datei de inceput trebuie sa fie mai mica sau la fel cu luna datei de sfarsit!");
      } else if(mm1 == mm2) {
        if (dd1 >= dd2) {
          console.log("Ziua datei de inceput trebuie sa fie strict mai mica decat ziua datei de sfarsit!");
          error.push("Ziua datei de inceput trebuie sa fie sctrict mai mica decat ziua datei de sfarsit!");
        }
      }
    }
    if (inceput > data_curenta || sfarsit < data_curenta) {
        console.log("Data inceput trebuie sa fie inaintea datei curente si data sfarsit dupa data curenta!");
        error.push("Data inceput trebuie sa fie inaintea datei curente si data sfarsit dupa data curenta!");
    }
    if (bilet.cnp.length != 13) {
      console.log("CNP-ul trebuie sa fie de 13 cifre!");
      error.push("CNP-ul trebuie sa fie de 13 cifre!");
    } else if (!bilet.cnp.match("^[0-9]+$")) {
      console.log("CNP-ul trebuie sa contina doar cifre!");
      error.push("CNP-ul trebuie sa contina doar cifre!");
    }
    if(bilet.cnp.substring(0, 1) == 1 || bilet.cnp.substring(0, 1) == 3 || bilet.cnp.substring(0, 1) == 5){
      bilet.gen = "M";
    } else if(bilet.cnp.substring(0, 1) == 2 || bilet.cnp.substring(0, 1) == 4 || bilet.cnp.substring(0, 1) == 6) {
      bilet.gen = "F";
    } else {
      console.log("CNP invalid!");
      error.push("CNP invalid!");
    }
    if (bilet.varsta.length < 1 && bilet.nume.length > 3) {
      console.log("Va rugam completati campul varsta corespunzator!");
      error.push("Va rugam completati campul varsta corespunzator!");
    } else if (!bilet.varsta.match("^[0-9]+$")) {
      console.log("Varsta trebuie sa contina doar cifre!");
      error.push("Varsta trebuie sa contina doar cifre!");
    } else {
      var data_nastere = new Date(bilet.cnp.substring(1, 3) + '/' + bilet.cnp.substring(3, 5) + '/' + (parseInt(bilet.cnp.substring(5, 7))+1)); 
      var diff =(data_curenta.getTime() - data_nastere.getTime()) / 1000;
      diff /= (60 * 60 * 24);
      var ani = Math.abs(parseInt(diff/365.25));
      if(ani != bilet.varsta) {
        console.log("Varsta nu corespunde cu data nasterii din CNP!");
        error.push("Varsta nu corespunde cu data nasterii din CNP!");
      } //Aici functioneaza, doar ca nu stie cate luni au exact lunile si ia si datele de genul 30 februarie din CNP sau nu recunoaste zilele de 31. Nu am aflat inca de ce.      
    }
    
    //Aici am incercat sa verific daca exista emailul care se vrea a fi introdus in baza de date, dar n-am reusit inca

    /*connection.query(`select * from abonamente_metrou where email="${req.query.email}" `, (err, result) => {
      if (err) throw err;
      if (result.length > 0){
        console.log("Acest email a mai fost introdus!");
        error.push("Acest email a mai fost introdus!");
      }
    });*/
  
  }

 
  if (error.length === 0) {

    const sql = 
    `INSERT INTO abonamente_metrou (nume, 
      prenume, 
      telefon, 
      cnp, 
      gen,
      varsta, 
      email, 
      data_inceput, 
      data_sfarsit) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.gen,
        bilet.varsta,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
